from random import choice, sample, seed
from tables import dnd5e, medievalnames, mazerats, neuralnetworknames, subroutines
seed()

OUTPUT_FILENAME = 'd&d characters.txt'
NUMBER_OF_CHARACTERS = 1000
# use these if you want to constrain one or more things, i.e. you want only random tieflings, or only random half-elf sorcerers
OVERRIDE_RACE = None
OVERRIDE_SUBRACE = None
OVERRIDE_CLASS = None
OVERRIDE_SUBCLASS = None
OVERRIDE_BACKGROUND = None

ROLLING_METHOD = 'array' # supports 'array' and '4d6b3'
ROLL_IN_ORDER = False
# comment out all but the name list you want to use
NAME_LIST = list(neuralnetworknames.NEURAL_NETWORK_NAMES)
# NAME_LIST = list(medievalnames.MEDIEVAL_EUROPEAN_NAMES)



CHARACTER_LEVEL = 1 #only 1st will be supported for foreseeable future
pc_fluff_traits = [] #todo: customise

assert (NUMBER_OF_CHARACTERS <= len(NAME_LIST)) #make fewer characters or use a longer name list

outputFile = open(OUTPUT_FILENAME, 'w')
for character in range(NUMBER_OF_CHARACTERS):
    pc_name = choice(NAME_LIST)
    NAME_LIST.remove(pc_name)
    #returns list of [race, subrace]
    pc_race = subroutines.getRaceAndSubrace()
    #choice() needs an indexable object, so keys() has to be cast to a list.
    pc_background = choice(list(dnd5e.CHARACTER_BACKGROUND_DICTIONARY.keys()))

    pc_class = choice(dnd5e.CHARACTER_CLASSES)
    #only returns subclass if it would be chosen at 1st level, otherwise returns None
    pc_subclass = subroutines.getSubclass(pc_class)

    if OVERRIDE_RACE:
        pc_race = subroutines.getRaceAndSubrace(OVERRIDE_RACE)
    if OVERRIDE_SUBRACE: #yes this allows subraces not of the parent race, like Gnome Blue Dragonborn
        pc_race[1] = OVERRIDE_SUBRACE
    if OVERRIDE_CLASS:
        pc_class = OVERRIDE_CLASS
    if OVERRIDE_SUBCLASS:
        pc_subclass = OVERRIDE_SUBCLASS
    if OVERRIDE_BACKGROUND:
        pc_background = OVERRIDE_BACKGROUND

    pc_proficiencies = subroutines.chooseSkills(pc_race, pc_background, pc_class, pc_subclass)

    #supports 'array', '4d6b3'
    rolled_scores = subroutines.rollAbilityScores(dnd5e.NUMBER_OF_SCORES, ROLLING_METHOD)
    if not ROLL_IN_ORDER:
        sorted_scores = subroutines.sortAbilityScores(rolled_scores, pc_class)
    else:
        sorted_scores = rolled_scores
    final_scores = subroutines.applyRacialAdjustments(sorted_scores, pc_race[1])
    spellcasting_ability_modifier = subroutines.getSpellcastingModifier(pc_class, final_scores)

    pc_fluff_traits.clear()
    pc_fluff_traits.extend([
        "Alignment: " + choice(dnd5e.DND_ALIGNMENT),
        "Appearance: " + choice(mazerats.APPEARANCE),
        "Physical Detail: " + choice(mazerats.PHYSICAL_DETAIL),
        "Clothing: " + choice(mazerats.CLOTHING),
        "Personality: " + choice(mazerats.PERSONALITY),
        "Mannerism: " + choice(mazerats.MANNERISM)
    ])

    if pc_class == 'Fighter':
        fighting_style = choice(dnd5e.FIGHTING_STYLES)
    else:
        fighting_style = None

    if pc_class == 'Ranger':
        #may be a 2-element list if initial choice is Humanoid
        favored_enemy = subroutines.getFavoredEnemy()
        favored_terrain = choice(dnd5e.FAVORED_TERRAINS)
    else:
        favored_enemy = None
        favored_terrain = None

    if pc_class == 'Rogue':
        expertise = sample(pc_proficiencies + ['thieves\' tools'], 2)
        expertise.sort()
    else:
        expertise = None

    if pc_class in dnd5e.SPELLS_DICT.keys():
        pc_spells = subroutines.chooseSpells(pc_class, pc_subclass, CHARACTER_LEVEL, spellcasting_ability_modifier)
    else:
        pc_spells = None

    statblock = str(character + 1) + '.\n'          \
              + pc_name + '\n'                      \
              + pc_race[1] + '\n'                   \
              + pc_background + ', ' + pc_class + ' ' + str(CHARACTER_LEVEL)
    if pc_subclass:
        statblock += ' (' + pc_subclass + ')'
    statblock += '\n'
    for score in range(dnd5e.NUMBER_OF_SCORES):
        statblock += dnd5e.ABILITY_SCORES[score] + ' ' + str(final_scores[score])
        if score != dnd5e.NUMBER_OF_SCORES - 1: #i.e. last ability score
            statblock += ', '
    statblock += '\n' \
               + 'Proficiencies: ' + ', '.join(pc_proficiencies) + '\n'

    if expertise:
        statblock += 'Expertise: {0}\n'.format(', '.join(expertise))
    if favored_enemy:
        statblock += 'Favored Enemy: {0}\n'.format(favored_enemy)
    if favored_terrain:
        statblock += 'Favored Terrain: {0}\n'.format(favored_terrain)
    if fighting_style:
        statblock += 'Fighting Style: {0}\n'.format(fighting_style)
    if pc_spells:
        statblock += 'Spells:\n'
        for level in pc_spells:
            if level:
                level.sort(key=str.lower)
                statblock += ', '.join(level) + '\n'

    for trait in pc_fluff_traits:
        statblock += trait + '\n'

    statblock += '\n'

    outputFile.write(statblock)
outputFile.close()
